# Brainstorm


## Requirements

What is a good Project Mission?

  - You have a deep knowledge of the domain
  - You have a genuine interest in the system
  - You can evaluate detailed requirements
  - The mission has interesting possibilities
  - The mission has a business case


## Our Role

We are a big company with lots of applicants. The recruiting process is slow,
tedious and 49%(?) of applications fail.


## Idea

Recruiting system

  - Make recruiting more efficient. Have less failed applications and save
    time/money while doing it.
  - Helping internal recruiting, finding the right people for a project
  - Automating external recruiting, at least parts of it, maybe ml
  - Instead of staking CVs, offer template for info to be filled in
  - Offer all the information to the recruiter in one click
  - Offer a lot of filters for applications
  - Have a website for applicants
  - Have an application for recruiters
  - answer all applicants with reasons why they were taken/not taken
